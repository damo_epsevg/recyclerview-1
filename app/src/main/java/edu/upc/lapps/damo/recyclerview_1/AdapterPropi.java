package edu.upc.lapps.damo.recyclerview_1;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Josep M on 19/06/2017.
 */

public class AdapterPropi extends RecyclerView.Adapter<AdapterPropi.RowHolder> {

    Activity context;

    String[] items;

    public AdapterPropi(Activity context, String[] dades) {
        items = dades;
        this.context = context;
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = context.getLayoutInflater().inflate(R.layout.fila, parent, false);
        return (new RowHolder(view));
    }

    @Override
    public void onBindViewHolder(RowHolder holder, final int position) {
        holder.poblaAmb(items[position]);
    }

    @Override
    public int getItemCount() {
        return (items.length);
    }

    class RowHolder extends RecyclerView.ViewHolder {
        TextView label = null;
        TextView size = null;
        ImageView icon = null;

        RowHolder(View row) {
            super(row);

            label = (TextView) row.findViewById(R.id.label);
            size = (TextView) row.findViewById(R.id.size);
            icon = (ImageView) row.findViewById(R.id.icon);
        }

        void poblaAmb(String item) {
            label.setText(item);
            size.setText("Size: " + item.length());

            if (item.length() <= 4) {
                icon.setImageResource(android.R.drawable.ic_delete);
            } else {
                icon.setImageResource(android.R.drawable.ic_input_add);
            }
        }

        @Override
        public String toString() {
            return super.toString();
        }

    }


}
